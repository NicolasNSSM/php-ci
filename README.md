WHAT
----
If you're working with PHP, this repository gives you a solid base to use Continuous Integration with Gitlab-CI, Docker and Capistrano

WHY
---
Because CI is the way to go and sharing may help some of you!

HOW
---
This repository has branches which correspond to a PHP project type :
* Symfony2 (PHP framework) branch shows how to use it with Gitlab-CI, Docker [php5.6, php7, mariadb] and Capistrano 
* Bolt3 (PHP CMS based on silex) branch shows how to use it with Gitlab-CI and Capistrano